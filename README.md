# Licenta BacMat

## Descriere
Acest joc 3D vine în ajutorul elevilor de liceu care urmează să susțină proba de matematică din cadrul examenului de bacalaureat. BacMat conține 18 întrebări teoretice în primul nivel și 13 întrebări aplicative în cel de-al doilea. Este o metodă plăcută dar eficientă pentru a repeta sau a învăța anumite concepte matematice curpinse în programă. 
Toate script-urile utilizate pentru realizarea acestuia se găsesc la: https://gitlab.upt.ro/oana.seracovanu/licenta-bacmat/-/tree/main/Scripts?ref_type=heads

## Instalare
Jocul poate fi instalat accesând: https://gitlab.upt.ro/oana.seracovanu/licenta-bacmat/-/tree/main/Game-%20.app%20file?ref_type=heads , dar este disponibil doar pentru MacOs. După descărcarea arhivei, aceasta se decomprimă și fișierul .app este disponibil. Acesta se accesează și pornește.

## Utilizare
Aplicația pornește cu scena de deschidere. După citirea informațiilor prezentate, prin apăsarea butonului de Start se accesează primul nivel. Se poate pune pe Pauză utilizând atât tastele P-pauză și R-repornire, cât și butoanele de pe ecran. De asemenea, se poate schimba perspectiva camerei utilizând tasta 1 pentru camera atașată personajului principal și tasta 2 pentru o privire de ansamblu ce îl include pe Lucy. Jocul presupune parcurgerea suprafeței de joc în căutarea ciupercilor. După ce o ciupercă este atinsă pe ecran apare o întrebare. Pentru un răspuns corect se obțin 10 puncte, în timp ce unul greșit duce la scăderea a 10 puncte. Avansarea la nivelul următor se face prin intrarea în peșteră, însă doar dacă s-au acumulat minim 100 de puncte. Altfel, scena va fi reluată complet. Atât ciupercile cât și întrebările dispar după ce au fost accesate o dată, evitând repetarea. 
