﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Collect_toadd : MonoBehaviour
{
    public Canvas canvas1; // Assign Canvas1 in the Inspector
    public Canvas canvas2; // Assign Canvas2 in the Inspector

    void start()
    {
        canvas1.gameObject.SetActive(true);
        canvas2.gameObject.SetActive(false);
    }
    void OnTriggerEnter(Collider other)
    {
        Destroy(gameObject);
        canvas1.gameObject.SetActive(false);
        canvas2.gameObject.SetActive(true);
    }

}
