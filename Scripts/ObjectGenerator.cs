﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectGenerator : MonoBehaviour
{
    public GameObject objectToGenerate; // The object you want to generate
    public Terrain terrain; // Reference to the terrain
    public int numberOfObjects = 10; // Number of objects to generate
    public float yOffset = 0.5f; // Offset from the terrain to avoid clipping

    private void Start()
    {
        GenerateObjectsOnTerrain();
    }

    void GenerateObjectsOnTerrain()
    {
        TerrainData terrainData = terrain.terrainData;
        Vector3 terrainSize = terrainData.size;

        for (int i = 0; i < numberOfObjects; i++)
        {
            // Generate a random position within the terrain's bounds
            Vector3 randomPosition = new Vector3(
                Random.Range(0f, terrainSize.x),
                yOffset,
                Random.Range(0f, terrainSize.z)
            );

            // Convert the random position to world space
            Vector3 worldPosition = terrain.transform.position + randomPosition;

            // Sample the terrain height at the random position
            float terrainHeight = terrain.SampleHeight(worldPosition);

            // Adjust the y-position based on the terrain height
            randomPosition.y = terrainHeight + yOffset;

            // Instantiate the object at the adjusted position
            GameObject newObject = Instantiate(objectToGenerate, randomPosition, Quaternion.identity);
        }
    }
}