﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class QuizManager : MonoBehaviour
{
    public List<QuestionsAndAnswers> QnA;
    public GameObject[] options;
    public int currentQuestion;
    public Text QuestionTxt;
    public Canvas canvas1; // Assign Canvas1 in the Inspector
    public Canvas canvas2; // Assign Canvas2 in the Inspector

    private void Start()
    {
        generateQuestion();
    }
    
    public void correct()
    {
        ScoreSystem.Score+=10;
        canvas1.gameObject.SetActive(true);
        canvas2.gameObject.SetActive(false);
        QnA.RemoveAt(currentQuestion);
        generateQuestion();

    }

    public void wrong()
    {
        ScoreSystem.Score-=10;
        canvas1.gameObject.SetActive(true);
        canvas2.gameObject.SetActive(false);
        QnA.RemoveAt(currentQuestion);
        generateQuestion();
    }

    void SetAnswers()
    {
        for(int i=0;i<options.Length;i++)
        {
            options[i].GetComponent<AnswerScript>().isCorrect=false;
            options[i].transform.GetChild(0).GetComponent<Text>().text= QnA[currentQuestion].Answers[i];
            if(QnA[currentQuestion].CorrectAnswer==i+1)
            {
                  options[i].GetComponent<AnswerScript>().isCorrect=true;
            }
        }
    }
    void generateQuestion()
    {
        if(QnA.Count>0)
        {
        currentQuestion=Random.Range(0, QnA.Count);
        QuestionTxt.text=QnA[currentQuestion].Question;
        SetAnswers();
        }

        else
        {
            Debug.Log("out of questions");
        }
    }
}
