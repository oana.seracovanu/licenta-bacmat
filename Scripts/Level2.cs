﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level2 : MonoBehaviour
{
    public string Scene_name;
    public string Scene_name2;
    public void OnTriggerEnter(Collider other)
    {
        if(ScoreSystem.Score>100 || ScoreSystem.Score==100)
        SceneManager.LoadScene(Scene_name);
        if(ScoreSystem.Score<100)
        {
        SceneManager.LoadScene(Scene_name2);
        ScoreSystem.Score=0;
        }
    }

    public void playGame()
    {
        SceneManager.LoadScene("Level1");
    }

    public void secondScene()
    {
         ScoreSystem.Score=0;
         SceneManager.LoadScene("Level2");
    }



}
