﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pause : MonoBehaviour
{
    private bool isPaused = false;
    public Button pauseButton;
    public Button resumeButton;


    private void Start()
{
    pauseButton.onClick.AddListener(PauseGame);
    resumeButton.onClick.AddListener(ResumeGame);
}
    void Update()
    {
        // Check for the "Pause" button input
        if (Input.GetKeyDown(KeyCode.R))
        {
            TogglePause();
        }

        if(Input.GetKeyDown(KeyCode.P))
        {
            ToggleResume();
        }
    }

    void TogglePause()
    {
        if (isPaused)
        {
            ResumeGame();
        }
    }
    
    void ToggleResume()
    {
        if(!isPaused)
        {
            PauseGame();
        }
    }

    void PauseGame()
    {
        Time.timeScale = 0; // Pause the game by setting timeScale to 0
        isPaused = true;
    }

    void ResumeGame()
    {
        Time.timeScale = 1; // Resume the game by setting timeScale back to 1
        isPaused = false;
    }
}
